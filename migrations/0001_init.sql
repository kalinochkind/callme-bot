CREATE TABLE users (
    tg_id integer not null primary key,
    is_staff integer not null,
    name text,
    state text not null
);

CREATE TABLE requested (
    id integer not null primary key autoincrement,
    tg_id integer not null,
    is_chat integer not null,
    message text not null,
    cancelled integer not null default 0,
    assigned_to integer,
    feedback integer,
    text_feedback text,
    foreign key (tg_id) references users(tg_id)
);

CREATE TABLE staff_request (
    tg_message_id integer not null,
    tg_chat_id integer not null,
    requested_id integer not null,
    foreign key (requested_id) references requested(id),
    primary key (tg_message_id, tg_chat_id)
);

CREATE INDEX staff_request_fk ON staff_request(requested_id);

CREATE TABLE attached_message (
    id integer not null primary key autoincrement,
    requested_id integer not null,
    message text not null,
    from_staff integer not null,
    foreign key (requested_id) references requested(id)
);

CREATE INDEX attached_message_fk ON attached_message(requested_id);
