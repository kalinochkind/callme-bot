import actions
import utils
import markups
from bot import bot
from config import TEXTS


@bot.message_handler(func=utils.match(state='feedback'))
def feedback(message):
    if message.text not in TEXTS['common']['feedback_options']:
        actions.add_text_feedback(message)
        actions.change_state(message, 'start')
        bot.send_message(message.chat.id, TEXTS['common']['feedback_after_more'],
                         reply_markup=markups.user_start_markup())
        return
    actions.add_feedback(message, TEXTS['common']['feedback_options'].index(message.text) + 1)
    actions.change_state(message, f'more_feedback {utils.get_request_id(message)}')
    bot.send_message(message.chat.id, TEXTS['common']['feedback_more'],
                     reply_markup=markups.more_feedback_markup())


@bot.message_handler(func=utils.match(state='more_feedback'))
def more_feedback(message):
    if message.text == TEXTS['common']['feedback_no_more_button']:
        actions.change_state(message, 'start')
        bot.send_message(message.chat.id, TEXTS['common']['feedback_after_no_more'],
                         reply_markup=markups.user_start_markup())
        return
    actions.add_text_feedback(message)
    actions.change_state(message, 'start')
    bot.send_message(message.chat.id, TEXTS['common']['feedback_after_more'],
                     reply_markup=markups.user_start_markup())
