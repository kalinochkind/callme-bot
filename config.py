import json

with open('texts.json') as f:
    TEXTS = json.load(f)

with open('config.json') as f:
    CONFIG = json.load(f)
