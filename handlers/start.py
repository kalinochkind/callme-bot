import utils
import actions
import markups
from bot import bot
from config import TEXTS


@bot.message_handler(commands=['start'], func=utils.match())
def start_command(message):
    if utils.pending_request_exists(message):
        bot.send_message(message.chat.id, TEXTS['common']['contact_reminder'], reply_markup=markups.cancel_markup())
        return
    if utils.is_in_chat(message):
        bot.send_message(message.chat.id, TEXTS['chat']['active_chat_reminder'], reply_markup=markups.remove_markup())
        return
    actions.change_state(message, 'start')
    bot.send_message(message.chat.id, TEXTS['start']['text'], reply_markup=markups.user_start_markup())


@bot.message_handler(func=utils.match(text=TEXTS['start']['chat_button'], state='start'))
def chat_button(message):
    actions.change_state(message, 'chat.requested')
    bot.send_message(message.chat.id, TEXTS['chat']['text'], reply_markup=markups.cancel_markup())


@bot.message_handler(func=utils.match(text=TEXTS['start']['call_button'], state='start'))
def call_button(message):
    actions.change_state(message, 'call.requested')
    bot.send_message(message.chat.id, TEXTS['call']['text'], reply_markup=markups.cancel_markup())


@bot.message_handler(func=utils.match(text=TEXTS['common']['cancel_button'], state={'chat.requested', 'call.requested'}))
def cancel_button(message):
    actions.change_state(message, 'start')
    bot.send_message(message.chat.id, TEXTS['common']['cancel_text'], reply_markup=markups.user_start_markup())


@bot.message_handler(commands=['help'])
@bot.message_handler(func=utils.match(text=TEXTS['start']['help_button'], state='start'))
def help_button(message):
    bot.send_message(message.chat.id, TEXTS['start']['help_text'])


@bot.message_handler(commands=['rmkb'])
def remove_keyboard(message):
    bot.send_message(message.chat.id, 'ok', reply_markup=markups.remove_markup())


@bot.message_handler(commands=['thischatid'])
def remove_keyboard(message):
    bot.send_message(message.chat.id, str(message.chat.id))
