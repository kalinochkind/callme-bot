import telebot

from config import TEXTS


def user_start_markup():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(telebot.types.KeyboardButton(TEXTS['start']['chat_button']))
    markup.add(telebot.types.KeyboardButton(TEXTS['start']['call_button']))
    markup.add(telebot.types.KeyboardButton(TEXTS['start']['help_button']))
    return markup


def cancel_markup():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(telebot.types.KeyboardButton(TEXTS['common']['cancel_button']))
    return markup


def empty_inline_markup():
    return telebot.types.InlineKeyboardMarkup()


def request_inline_markup(request_id):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(TEXTS['staff']['respond_button'], callback_data=f"resp_{request_id}"))
    return markup


def request_approve_inline_markup(request_id):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton('Да', callback_data=f"respyes_{request_id}"),
               telebot.types.InlineKeyboardButton('Нет', callback_data=f"respno_{request_id}"))
    return markup


def feedback_markup():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(*[telebot.types.KeyboardButton(s)
                 for i, s in enumerate(TEXTS['common']['feedback_options'])])
    return markup


def more_feedback_markup():
    markup = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)
    markup.add(telebot.types.KeyboardButton(TEXTS['common']['feedback_no_more_button']))
    return markup


def remove_markup():
    return telebot.types.ReplyKeyboardRemove()
