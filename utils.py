import telebot

import db
from config import CONFIG


def any_startswith(text, prefixes):
    return any(text == p or text.startswith(p + ' ') for p in prefixes)


def ensure_state(message):
    if not hasattr(message, '_user_state') or not hasattr(message, '_user_is_staff'):
        message._user_is_staff, message._user_state = db.get_user_state(message.from_user.id)


def compare_state(actual, expected):
    if isinstance(expected, str):
        expected = {expected}
    return any_startswith(actual, expected)


def match(*, text=None, state=None, is_staff=False, data_prefix=None):
    def compare(message):
        if isinstance(message, telebot.types.Message) and message.chat.id in CONFIG['staff_chats']:
            return False
        ensure_state(message)
        if bool(is_staff) != message._user_is_staff:
            return False
        if text is not None and message.text != text:
            return False
        if state is not None and not compare_state(message._user_state, state):
            return False
        if data_prefix is not None and not message.data.startswith(data_prefix):
            return False
        return True
    return compare


def build_request_text(text, is_chat):
    return ('[ЧАТ]\n\n' if is_chat else '[ЗВОНОК]\n\n') + text


def get_request_id(message):
    ensure_state(message)
    try:
        return int(message._user_state.split()[1])
    except IndexError:
        return None


def pending_request_exists(message):
    ensure_state(message)
    return any_startswith(message._user_state, ['chat.waiting', 'call.waiting'])


def is_in_chat(message):
    ensure_state(message)
    return any_startswith(message._user_state, ['chat.active', 'call.active'])


def is_staff_busy(message):
    ensure_state(message)
    return any_startswith(message._user_state, ['chat', 'call'])
