import sqlite3


connection = sqlite3.Connection('db.sqlite3')


# noinspection PyDefaultArgument
def ensure_user_exists(tg_id, existing_users=set()):
    if tg_id not in existing_users:
        cursor = connection.cursor()
        cursor.execute('INSERT OR IGNORE INTO users(tg_id, is_staff, state) VALUES (?, 0, \'start\')', (tg_id,))
        existing_users.add(tg_id)
        connection.commit()


def set_user_state(tg_id, state):
    ensure_user_exists(tg_id)
    cursor = connection.cursor()
    cursor.execute('UPDATE users SET state=? WHERE tg_id=?', (state, tg_id))
    connection.commit()


def get_user_state(tg_id):
    ensure_user_exists(tg_id)
    cursor = connection.cursor()
    cursor.execute('SELECT is_staff, state FROM users WHERE tg_id=?', (tg_id,))
    data = cursor.fetchone()
    return bool(data[0]), data[1]


def get_user_name(tg_id):
    cursor = connection.cursor()
    cursor.execute('SELECT name FROM users WHERE tg_id=?', (tg_id,))
    return cursor.fetchone()[0]


def add_requested_contact(tg_id, message, is_chat):
    ensure_user_exists(tg_id)
    cursor = connection.cursor()
    cursor.execute('INSERT INTO requested(tg_id, is_chat, message) VALUES (?, ?, ?)', (tg_id, int(is_chat), message))
    request_id = cursor.lastrowid
    connection.commit()
    return request_id


def make_request_cancelled(request_id):
    cursor = connection.cursor()
    cursor.execute('UPDATE requested SET cancelled=1 WHERE id=? AND cancelled=0 AND assigned_to IS NULL', (request_id,))
    connection.commit()
    return cursor.lastrowid > 0


def add_contact_staff_messages(request_id, staff_messages):
    cursor = connection.cursor()
    for message in staff_messages:
        cursor.execute('INSERT INTO staff_request(tg_message_id, tg_chat_id, requested_id) VALUES (?, ?, ?)',
                       (message.message_id, message.chat.id, request_id))
    connection.commit()


def get_contact_staff_messages(request_id):
    cursor = connection.cursor()
    cursor.execute('SELECT tg_message_id, tg_chat_id FROM staff_request WHERE requested_id=?', (request_id,))
    return list(cursor.fetchall())


def add_attached_message(request_id, message, from_staff=False):
    cursor = connection.cursor()
    cursor.execute('INSERT INTO attached_message(requested_id, message, from_staff) VALUES (?, ?, ?)',
                   (request_id, message, int(from_staff)))
    connection.commit()


def get_request_info(request_id):
    cursor = connection.cursor()
    cursor.execute('SELECT tg_id, message, is_chat, assigned_to FROM requested WHERE id=?', (request_id,))
    data = cursor.fetchone()
    if data is None:
        return {'user': None, 'message': None, 'is_chat': None, 'assignee': None}
    return {'user': data[0], 'message': data[1], 'is_chat': data[2], 'assignee': data[3]}


def set_request_assignee(request_id, user_id):
    cursor = connection.cursor()
    cursor.execute('UPDATE requested SET assigned_to=? WHERE id=? AND assigned_to IS NULL AND cancelled=0',
                   (user_id, request_id))
    connection.commit()
    return cursor.rowcount > 0


def get_attached_messages(request_id):
    cursor = connection.cursor()
    cursor.execute('SELECT message FROM attached_message WHERE requested_id=? ORDER BY id', (request_id,))
    return [i[0] for i in cursor.fetchall()]


def set_request_feedback(request_id, mark):
    cursor = connection.cursor()
    cursor.execute('UPDATE requested SET feedback=? WHERE id=?', (mark, request_id))
    connection.commit()


def set_request_text_feedback(request_id, text):
    cursor = connection.cursor()
    cursor.execute('UPDATE requested SET text_feedback=? WHERE id=?', (text, request_id))
    connection.commit()
