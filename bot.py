import telebot

from config import CONFIG


if CONFIG.get('proxy'):
    telebot.apihelper.proxy = CONFIG['proxy']

bot = telebot.TeleBot(CONFIG['token'], threaded=False)
