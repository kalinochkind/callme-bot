import db
import markups
import utils
from bot import bot
from config import CONFIG, TEXTS


def change_state(message, state):
    message._user_state = state
    db.set_user_state(message.from_user.id, state)


def send_contact_staff_messages(is_chat, request_id):
    ids = []
    text = db.get_request_info(request_id)['message']
    request_text = utils.build_request_text(text, is_chat)
    for chat in CONFIG['staff_chats']:
        ids.append(bot.send_message(chat, request_text,
                                    reply_markup=markups.request_inline_markup(request_id)))
    db.add_contact_staff_messages(request_id, ids)


def request_contact(message, is_chat):
    request_id = db.add_requested_contact(message.chat.id, message.text, is_chat)
    if is_chat:
        send_contact_staff_messages(is_chat, request_id)
    return request_id


def attach_contact_message(message):
    request_id = utils.get_request_id(message)
    assert request_id is not None
    db.add_attached_message(request_id, message.text)


def finalize_call_request(message):
    attach_contact_message(message)
    request_id = utils.get_request_id(message)
    assert request_id is not None
    send_contact_staff_messages(False, request_id)
    return request_id


def remove_respond_buttons(request_id):
    for msg_id, chat_id in db.get_contact_staff_messages(request_id):
        bot.edit_message_reply_markup(chat_id, msg_id, markups.empty_inline_markup())


def cancel_request(message):
    request_id = utils.get_request_id(message)
    if request_id is None:
        return
    if db.make_request_cancelled(request_id):
        remove_respond_buttons(request_id)


def assign_request(message, request_id):
    if not db.set_request_assignee(request_id, message.from_user.id):
        return False
    info = db.get_request_info(request_id)
    db.set_user_state(message.from_user.id, f"{'chat' if info['is_chat'] else 'call'} {request_id}")
    remove_respond_buttons(request_id)
    bot.send_message(message.from_user.id, TEXTS['staff']['taken_chat' if info['is_chat'] else 'taken_call'],
                     reply_markup=markups.remove_markup())
    db.set_user_state(info['user'], f'{"chat" if info["is_chat"] else "call"}.active {request_id}')

    for attached in db.get_attached_messages(request_id):
        bot.send_message(message.from_user.id, attached)
    name = db.get_user_name(message.from_user.id)
    bot.send_message(info['user'],
                     TEXTS['chat' if info['is_chat'] else 'call']['found_text'].replace('$name', str(name)),
                     reply_markup=markups.remove_markup())
    return True


def send_feedback_message(tg_id, is_chat):
    bot.send_message(tg_id, TEXTS['chat' if is_chat else 'call']['feedback_text'],
                     reply_markup=markups.feedback_markup())


def add_feedback(message, mark):
    request_id = utils.get_request_id(message)
    db.set_request_feedback(request_id, mark)


def add_text_feedback(message):
    request_id = utils.get_request_id(message)
    db.set_request_text_feedback(request_id, message.text)


def send_chat_message(message):
    request_id = utils.get_request_id(message)
    info = db.get_request_info(request_id)
    # TODO картинки
    if message.from_user.id == info['assignee']:
        db.add_attached_message(request_id, message.text, True)
        bot.send_message(info['user'], message.text)
    elif message.from_user.id == info['user']:
        db.add_attached_message(request_id, message.text)
        bot.send_message(info['assignee'], message.text)


def finish_chat(message):
    request_id = utils.get_request_id(message)
    info = db.get_request_info(request_id)
    if message.from_user.id == info['assignee']:
        bot.send_message(info['assignee'], TEXTS['chat']['finished_by_me_text'])
        bot.send_message(info['user'], TEXTS['chat']['finished_by_other_text'])
    elif message.from_user.id == info['user']:
        bot.send_message(info['assignee'], TEXTS['chat']['finished_by_other_text'])
        bot.send_message(info['user'], TEXTS['chat']['finished_by_me_text'])
    db.set_user_state(info['assignee'], '')
    db.set_user_state(info['user'], f'feedback {request_id}')
    send_feedback_message(info['user'], info['is_chat'])


def send_assignee_confirmation(request_id, tg_id):
    info = db.get_request_info(request_id)
    if info['user'] is None:
        return
    text = utils.build_request_text(info['message'], info['is_chat'])
    bot.send_message(tg_id, text + '\n\n' + TEXTS['staff']['respond_text'],
                     reply_markup=markups.request_approve_inline_markup(request_id))
