import utils
import markups
import actions
from bot import bot
from config import TEXTS


@bot.callback_query_handler(func=utils.match(is_staff=True, data_prefix='resp'))
def respond(call):
    if call.data.startswith('resp_'):
        if utils.is_staff_busy(call):
            bot.answer_callback_query(call.id, TEXTS['staff']['busy'])
            return
        request_id = int(call.data[5:])
        actions.send_assignee_confirmation(request_id, call.from_user.id)
    elif call.data.startswith('respno_'):
        bot.edit_message_reply_markup(call.from_user.id, call.message.message_id, markups.empty_inline_markup())
    elif call.data.startswith('respyes_'):
        request_id = call.data[8:]
        bot.edit_message_reply_markup(call.from_user.id, call.message.message_id, markups.empty_inline_markup())
        if not actions.assign_request(call, request_id):
            bot.send_message(call.from_user.id, TEXTS['staff']['expired'])
    bot.answer_callback_query(call.id, None)


@bot.message_handler(commands=['finish'], func=utils.match(state=['chat', 'call'], is_staff=True))
def finish_chat_command(message):
    actions.finish_chat(message)


@bot.message_handler(func=utils.match(state=['chat', 'call'], is_staff=True))
def chat_message(message):
    actions.send_chat_message(message)
