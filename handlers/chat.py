import utils
import actions
import markups
from bot import bot
from config import TEXTS


@bot.message_handler(func=utils.match(state='chat.requested'))
def chat_message(message):
    request_id = actions.request_contact(message, True)
    actions.change_state(message, f'chat.waiting {request_id}')
    bot.send_message(message.chat.id, TEXTS['chat']['requested_text'], reply_markup=markups.cancel_markup())


@bot.message_handler(func=utils.match(text=TEXTS['common']['cancel_button'], state='chat.waiting'))
def chat_cancel(message):
    actions.cancel_request(message)
    actions.change_state(message, 'start')
    bot.send_message(message.chat.id, TEXTS['common']['cancel_text'], reply_markup=markups.user_start_markup())


@bot.message_handler(func=utils.match(state='chat.waiting'))
def chat_extra_message(message):
    actions.attach_contact_message(message)


@bot.message_handler(commands=['finish'], func=utils.match(state='chat.active'))
def finish_chat_command(message):
    actions.finish_chat(message)


@bot.message_handler(func=utils.match(state='chat.active'))
def chat_message(message):
    actions.send_chat_message(message)
