import utils
import markups
import actions
from bot import bot
from config import TEXTS


@bot.message_handler(func=utils.match(state='call.requested'))
def call_message(message):
    request_id = actions.request_contact(message, False)
    actions.change_state(message, f'call.contacts {request_id}')
    bot.send_message(message.chat.id, TEXTS['call']['contact_text'], reply_markup=markups.cancel_markup())


@bot.message_handler(func=utils.match(text=TEXTS['common']['cancel_button'], state=['call.waiting', 'call.contacts']))
def call_cancel(message):
    actions.cancel_request(message)
    actions.change_state(message, 'start')
    bot.send_message(message.chat.id, TEXTS['common']['cancel_text'], reply_markup=markups.user_start_markup())


@bot.message_handler(func=utils.match(state='call.contacts'))
def call_contact(message):
    request_id = actions.finalize_call_request(message)
    actions.change_state(message, f'call.waiting {request_id}')
    bot.send_message(message.chat.id, TEXTS['call']['requested_text'], reply_markup=markups.cancel_markup())


@bot.message_handler(func=utils.match(state='call.waiting'))
def call_extra_message(message):
    actions.attach_contact_message(message)


@bot.message_handler(commands=['finish'], func=utils.match(state='call.active'))
def finish_call_command(message):
    actions.finish_chat(message)


@bot.message_handler(func=utils.match(state='call.active'))
def call_message(message):
    actions.send_chat_message(message)
